import init from "./init.js";
import loader from "./loader.js";
import store from "./store.js";

export default {
    init, loader, store
};
