import globals from "aktivitet/globals";

export default function loader( $activity, options ) {
    var aid = globals.dummy_store.get_aid( $activity );

    if ( aid && globals.dummy_store.has( aid ) )
    {
        return globals.dummy_store.get( aid );
    }
    return {};
};