class DummyStore
{
    constructor( id, type = 'map' )
    {
        this.id = id;
        this.type = type;
        this.map = new Map();

        switch ( this.type )
        {
            case 'session':
                this.store = window.sessionStorage
            break;
            case 'local':
                this.store = window.localStorage
            break;
            default:
                this.store = null;
        }

        this.load();
    }

    get_aid( $activity )
    {
        return $activity.dataset.aid || $activity.id || null;
    }

    load()
    {
        var states;

        if ( this.store )
        {
            states = JSON.parse( this.store.getItem( this.id ) );

            if ( states )
                this.map = new Map( Object.entries( states ) );
        }

    }

    save()
    {
        var states;

        if ( this.store )
        {
            states = Object.fromEntries( this.map.entries() );

            this.store.setItem( this.id, JSON.stringify( states ) );
        }
    }

    handle_state_ev( e )
    {
        var aid = this.get_aid( e.target );
        // require id
        if ( ! aid )
            return;

        var state = e.target.activity.save();

        this.set( aid, state );
    }

    set( k, v )
    {
        this.map.set( k, v );
        this.save();
    }

    get( k )
    {
        return this.map.get( k );
    }

    has( k )
    {
        return this.map.has( k );
    }
}

export default DummyStore;
