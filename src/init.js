import globals from "aktivitet/globals";
import store from './store.js';
import loader from './loader.js';

const default_events = [
    'started', 'completed', 'unlocked',
];

export default function ( id, type = 'map', events = null ) {
    globals.dummy_store = new store( id, type );

    for ( var evi in default_events )
    {
        globals.$event_root.addEventListener( default_events[ evi ], globals.dummy_store.handle_state_ev.bind( globals.dummy_store ) );
    }

    globals.loader = loader;
};
